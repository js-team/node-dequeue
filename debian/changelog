node-dequeue (1.0.5-6) unstable; urgency=medium

  * Fix autopkgtest

 -- Yadd <yadd@debian.org>  Sat, 04 Dec 2021 23:05:27 +0100

node-dequeue (1.0.5-5) unstable; urgency=medium

  * Update standards version to 4.6.0, no changes needed
  * Add "Rules-Requires-Root: no"
  * Add debian/gbp.conf
  * Modernize debian/watch
  * Fix filenamemangle
  * Fix GitHub tags regex
  * Use dh-sequence-nodejs instead of pkg-js-tools and fix install
  * Drop dependency to nodejs

 -- Yadd <yadd@debian.org>  Sun, 21 Nov 2021 14:04:35 +0100

node-dequeue (1.0.5-4) unstable; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 15 Sep 2021 11:56:50 +0100

node-dequeue (1.0.5-3) unstable; urgency=low

  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * Apply multi-arch hints.
    + node-dequeue: Add Multi-Arch: foreign.
  * Remove 1 unused lintian overrides.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 28 Aug 2021 22:52:53 +0100

node-dequeue (1.0.5-2) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/control: Drop myself from Uploaders: field.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 11
  * Declare compliance with policy 4.3.0
  * Change section to javascript
  * Change priority to optional
  * Update VCS fields to salsa
  * Update debian/copyright
  * Add myself to uploaders (Closes: #921357)
  * Add upstream/metadata
  * Add minimal test

 -- Xavier Guimard <yadd@debian.org>  Tue, 05 Feb 2019 14:50:30 +0100

node-dequeue (1.0.5-1) unstable; urgency=medium

  * New upstream release.
  * debian/rules:
    + Add get-orig-source rule.
  * debian/control:
    + Move packaging Git to pkg-javascript namespace on Alioth.
    + Alioth-canonicalize Vcs-*: fields.
    + Bump Standards: to 3.9.5. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 19 Aug 2014 19:41:42 +0200

node-dequeue (1.0.3-2) unstable; urgency=low

  * /debian/control:
    + Fix versioned dependency on nodejs.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 15 May 2013 11:12:53 +0200

node-dequeue (1.0.3-1) unstable; urgency=low

  * Initial release. (Closes: #707656).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 10 May 2013 01:45:31 +0200
